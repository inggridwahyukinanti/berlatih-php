<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<?php
function ubah_huruf($string){

    $aksara = "abcdefghijklmnopqrstuvwxyz";
    $ditampung = "";
    for ($k=0 ; $k < strlen($string) ; $k++){
        $posisi = strpos($aksara, $string[$k]);
        $ditampung .= substr($aksara, $posisi + 1, 1); //jadi
    }
return $ditampung ."<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>

</body>
</html>