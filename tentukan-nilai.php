<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<?php
function tentukan_nilai($number)
{
    if ($number >= 85 and $number <= 100) {
        echo "Sangat Baik";
    } elseif ($number >= 70 and $number < 85){
        echo "Baik";
    } elseif ($number >= 60 and $number < 70 ){
        echo "Cukup";
    }else{
        echo "Kurang";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo"<br>";
echo tentukan_nilai(76); //Baik
echo"<br>";
echo tentukan_nilai(67); //Cukup
echo"<br>";
echo tentukan_nilai(43); //Kurang
?>



</body>
</html>